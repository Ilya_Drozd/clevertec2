package org.clevertec.project.filter;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Date;

@Data
public class FilterForHistory {
    private String[] userNames;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date timestampFrom;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date timestampTo;
    private String[] operatingType;
    private String[] entityType;
    private Boolean[] isWaslStatus;
    private Boolean[] isWialonStatus;

    public Query generalCriterion(Query query){
        query = nameCriteria(query);
        //query = timeCriteria(query);
        query = operatingTypeCriteria(query);
        query = entityTypeCriteria(query);
        query = isWaslStatusCriteria(query);
        query = isWialonStatusCriteria(query);
        return query;
    }

    private Query nameCriteria(Query query){
        if (userNames.length != 0){
            query.addCriteria(Criteria.where("userName").in(userNames));
        }
        return query;
    }

//    private Query timeCriteria(Query query){
////        if (timestampFrom != null && timestampTo != null){
//            //query.addCriteria(Criteria.where("timestampFrom").gt(timestampFrom));
//            query.addCriteria(Criteria.where("timestampTo").lt(timestampTo));
////        } else if (timestampFrom != null) {
////            query.addCriteria(Criteria.where("timestampFrom").gt(timestampFrom));
////        } else if (timestampTo != null) {
////            query.addCriteria(Criteria.where("timestampTo").lt(timestampTo));
////        }
//        return query;
//    }

    private Query operatingTypeCriteria(Query query){
        if (operatingType.length != 0){
            query.addCriteria(Criteria.where("operatingType").in(operatingType));
        }
        return query;
    }

    private Query entityTypeCriteria(Query query){
        if (entityType.length != 0){
            query.addCriteria(Criteria.where("entityType").in(entityType));
        }
        return query;
    }

    private Query isWaslStatusCriteria(Query query){
        if (isWaslStatus.length != 0){
            query.addCriteria(Criteria.where("isWaslStatus").in(isWaslStatus));
        }
        return query;
    }

    private Query isWialonStatusCriteria(Query query){
        if (isWialonStatus.length != 0){
            query.addCriteria(Criteria.where("isWialonStatus").in(isWialonStatus));
        }
        return query;
    }
}
