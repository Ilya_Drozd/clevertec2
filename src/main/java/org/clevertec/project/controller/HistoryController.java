package org.clevertec.project.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.clevertec.project.dto.History;
import org.clevertec.project.filter.FilterForHistory;
import org.clevertec.project.service.HistoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/history")
@Slf4j
@RequiredArgsConstructor
public class HistoryController {

    private final HistoryService service;

    @PostMapping("/add")
    public ResponseEntity<?> save(@RequestBody History history){
        try{
            return ResponseEntity.ok(service.save(history));
        } catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/get/some")
    public ResponseEntity<?> findSome(@RequestParam(name = "pagesize", defaultValue = "15", required = false)
                                                  Integer pageSize,
                                      @RequestParam(name = "pagenumber", defaultValue = "0", required = false)
                                                  Integer pageNumber,
                                      @RequestBody FilterForHistory filter){
        try{
            return ResponseEntity.ok(service.findSome(pageSize, pageNumber, filter));
        } catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/get/all")
    public ResponseEntity<?> findAll(
            @RequestParam(name = "pagesize", defaultValue = "15", required = false)
                    Integer pageSize,
            @RequestParam(name = "pagenumber", defaultValue = "0", required = false)
                    Integer pageNumber){
        try{
            return ResponseEntity.ok(service.findAll(pageSize, pageNumber));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/all")
    public ResponseEntity<?> deleteAll(){
        try{
            return ResponseEntity.ok(service.deleteAll());
        } catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
