package org.clevertec.project.service;

import org.clevertec.project.dto.History;
import org.clevertec.project.filter.FilterForHistory;
import org.springframework.data.domain.Page;

import java.util.List;

public interface HistoryService {
    String save(History history);
    Page<History> findAll(Integer pageSize, Integer pageNumber);
    Page<History> findSome(Integer pageSize, Integer pageNumber, FilterForHistory filter);
    int deleteAll();
}
