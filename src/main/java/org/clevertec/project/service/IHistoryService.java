package org.clevertec.project.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.clevertec.project.dto.History;
import org.clevertec.project.filter.FilterForHistory;
import org.clevertec.project.repository.HistoryRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class IHistoryService implements HistoryService{

    private final HistoryRepository repository;
    private final ObjectMapper objectMapper;
    private final MongoTemplate mongoTemplate;

    @Override
    public String save(History history) {
        repository.save(history);
        return history.getId();
    }

    @Override
    public Page<History> findAll(Integer pageSize, Integer pageNumber) {
        Page<History> histories = repository.findAll(PageRequest.of(pageNumber, pageSize));
        log.info("History pageable received successful : {}", toJson(histories));
        return histories;
    }

    @Override
    public Page<History> findSome(Integer pageSize, Integer pageNumber, FilterForHistory filter) {
        final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);

        Query query = new Query();
        query = filter.generalCriterion(query);
        query.with(pageableRequest);

        List<History> list = mongoTemplate.find(query, History.class);
        return new PageImpl<>(list, pageableRequest, list.size());
    }

    @Override
    public int deleteAll() {
        List<History> list = repository.findAll();
        repository.deleteAll();
        return list.size();
    }

    private synchronized String toJson(Object o) {
        String json = null;
        try {
            json = objectMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            log.warn("can't represent object of class {} in json form for logging: {}", o.getClass().getSimpleName(), e.toString());
        }
        return json;
    }
}
